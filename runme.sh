#!/bin/bash

DEVEL_PATH=$HOME/devel/sympatic/sample-argparse
VENV_PATH=$DEVEL_PATH/venv

CONF_FILE=$DEVEL_PATH/fake-conf.yaml

$VENV_PATH/bin/python $DEVEL_PATH/main.py "$@"


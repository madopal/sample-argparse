#!/usr/bin/env python3
"""
Utility to show parsing of arguments and use
of environment variables for configuration
"""

import os
import sys
from argparse import ArgumentParser
import json

# this is loaded when the file is run *or* imported, and it'll default
# to a filename if it can't find the environment var
DEFAULT_CONF_FILE = os.environ.get('CONF_FILE', 'fake-conf.json')

def parse_cmd_args():
    """ Routine to setup argparse """
    parser = ArgumentParser()
    parsers = parser.add_subparsers(title='command',
                                    description='valid commands',
                                    dest='command')
    # create-user
    create_user = parsers.add_parser('test-1')
    create_user.add_argument('-c', '--conf-file',
                             help='configuration file location',
                             default=DEFAULT_CONF_FILE)
    create_user.add_argument('-n', '--name',
                             help='user name',
                             required=True)
    create_user.add_argument('-e', '--email',
                             help='email',
                             required=True)
    args = parser.parse_args()
    return args

def main(args=None):
    """ Main routine 
    """
    # The reason to pass the args is that you can
    # now call this or import this if you want to
    # run it as a test
    print(args)
    with open(args.conf_file, 'r') as in_file:
        json_conf = json.load(in_file)
        print("Conf file data: {}".format(json_conf))

if __name__ == '__main__':
    args = parse_cmd_args()
    main(args=args)

